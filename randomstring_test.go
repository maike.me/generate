package generate

import "testing"

func TestRandomString(t *testing.T) {
	for i := 0; i < 25; i++ {
		str := RandomString(i)
		if len(str) != i {
			t.Errorf("RandomString(%d) len = %d want %d", i, len(str), i)
		}
	}
}
